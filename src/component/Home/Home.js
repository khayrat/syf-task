import React, { useEffect, useState, useCallback } from "react";
import { Table, Divider, Button, Select } from "antd";
import moment from "moment";
import {
  DeleteFilled,
  EditFilled,
  EyeFilled,
  FilterFilled,
} from "@ant-design/icons";

import {
  getAllTask,
  deleteSingleTask,
  searchTask,
  updateTask,
} from "../../taskAPI";
import { STATUS_MAP } from "../../Constant/constant";

import DetailsModal from "../DetailsModal/DetailsModal";
import EditModal from "../EditModal/EditModal";

const Home = (props) => {
  const [getAllDate, setGetAllDate] = useState([]);
  const [searchValue, setSearchValue] = useState(null);
  const [visibleDetails, setVisibleDetails] = useState(false);
  const [loading, setLoading] = useState(false);
  const [itemId, setItemId] = useState(null);
  const [visibleEdit, setVisibleEdit] = useState(false);

  const columns = [
    {
      title: "Task Id",
      dataIndex: "id",
    },
    {
      title: "Customer Name",
      dataIndex: "customer_name",
    },
    {
      title: "Customer Phone",
      dataIndex: "customer_phone",
    },
    {
      title: "Email",
      dataIndex: "customer_email",
    },
    {
      title: "Task Status",
      dataIndex: "task_status",
      render: (status) => {
        const status_item = STATUS_MAP.find((item) => item.value === status);
        return status_item ? status_item.label : status;
      },
    },
    {
      title: "Task Date",
      dataIndex: "task_datetime",
      render: (dateTime) => moment(dateTime).format("DD MMM YYYY - hh:mm A"),
    },
    {
      title: "View",
      render: (_, record) => (
        <Button
          onClick={() => {
            setItemId(record.id);
            setVisibleDetails(true);
          }}
        >
          <EyeFilled /> View
        </Button>
      ),
    },
    {
      title: "Edit",
      render: (_, record) => (
        <Button
          onClick={() => {
            setItemId(record.id);
            setVisibleEdit(true);
          }}
        >
          <EditFilled /> Edit
        </Button>
      ),
    },
    {
      title: "Delete",
      render: (_, record) => (
        <Button onClick={() => handleDeleteClick(record)}>
          <DeleteFilled /> Delete
        </Button>
      ),
    },
  ];

  const getdateTable = async () => {
    const response = await getAllTask();
    setGetAllDate(response);
    setLoading(false);
  };

  const handleDeleteClick = useCallback(async (item) => {
    setLoading(true);
    const response = await deleteSingleTask(item.id);
    if (response && !searchValue) {
      getdateTable();
    } else {
      handleSearch();
    }
  });

  useEffect(() => {
    setLoading(true);
    getdateTable();
  }, []);

  const handleChange = (value) => {
    setSearchValue(value);
  };

  const handleSearch = useCallback(async () => {
    setLoading(true);
    const response = await searchTask(searchValue);
    setGetAllDate(response);
    setLoading(false);
  });

  const handleOk = () => {
    setVisibleDetails(false);
    setItemId(null);
  };

  const handleEditOk = async (id, value) => {
    const response = await updateTask(id, value);
    if (response && !searchValue) {
      getdateTable();
    } else {
      handleSearch();
    }
    setVisibleEdit(false);
    setItemId(null);
  };

  const handleEditCancel = () => {
    setVisibleEdit(false);
    setItemId(null);
  };

  return (
    <div>
      <DetailsModal
        visible={visibleDetails}
        itemId={itemId}
        handleOk={handleOk}
      />
      <EditModal
        visible={visibleEdit}
        itemId={itemId}
        handleOk={handleEditOk}
        handleCancel={handleEditCancel}
      />
      <Divider />
      <Select
        mode="multiple"
        style={{ width: "50%" }}
        placeholder="Please select"
        onChange={handleChange}
        options={STATUS_MAP}
      />
      <Button onClick={() => handleSearch()}>
        <FilterFilled /> search
      </Button>
      <Divider />

      <Table
        rowKey="id"
        columns={columns}
        loading={loading}
        dataSource={getAllDate || []}
      />
    </div>
  );
};

export default Home;
