import React, { useEffect, useState } from "react";
import { Modal, Button } from "antd";
import moment from "moment";

import { getSingleTask } from "../../taskAPI";
import { STATUS_MAP } from "../../Constant/constant";

import "./Details.css";
const DetailsModal = (props) => {
  const { visible, handleOk, itemId } = props;

  const [singleDate, setSingleDate] = useState([]);

  const getDetailsTask = async () => {
    const response = await getSingleTask(itemId);
    setSingleDate(response);
  };

  useEffect(() => {
    if (itemId !== null && visible) getDetailsTask();
  }, [itemId]);

  return (
    <Modal
      title="Details"
      visible={visible}
      onOk={handleOk}
      cancelText={null}
      closable={false}
      footer={[
        <Button key={singleDate.id} type="primary" onClick={handleOk}>
          Ok
        </Button>,
      ]}
    >
      <div className="br-modal-content">
        <div className="br-modal-content__item">
          <div>Task Id:</div>
          <div>{singleDate.id}</div>
        </div>
        <div className="br-modal-content__item">
          <div>Customer Name:</div>
          <div>{singleDate.customer_name}</div>
        </div>
        <div className="br-modal-content__item">
          <div>Customer Phone:</div>
          <div>{singleDate.customer_phone}</div>
        </div>
        <div className="br-modal-content__item">
          <div>Customer Email:</div>
          <div>{singleDate.customer_email}</div>
        </div>
        <div className="br-modal-content__item">
          <div>Task Status:</div>
          <div>
            {STATUS_MAP.find((item) => item.value === singleDate.task_status)
              ? STATUS_MAP.find((item) => item.value === singleDate.task_status)
                  .label
              : singleDate.task_status}
          </div>
        </div>

        <div className="br-modal-content__item">
          <div>task date:</div>
          <div>
            {moment(singleDate.task_datetime).format("DD MMM YYYY - hh:mm A")}
          </div>
        </div>

        <div className="br-modal-content__item">
          <div>barcode:</div>
          <div>{singleDate.barcode} </div>
        </div>
      </div>
    </Modal>
  );
};

export default DetailsModal;
