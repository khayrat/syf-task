import React, { useEffect, useRef } from "react";
import { Modal, Input, Form, Select, DatePicker } from "antd";
import moment from "moment";

import { getSingleTask } from "../../taskAPI";
import { STATUS_MAP } from "../../Constant/constant";

import "./EditModal.css";
const EditModal = (props) => {
  const { visible, handleOk, itemId, handleCancel } = props;

  const formRef = useRef(null);
  const getDetailsTask = async () => {
    const response = await getSingleTask(itemId);
    formRef.current.setFieldsValue({
      customer_name: response.customer_name,
      customer_phone: response.customer_phone,
      customer_email: response.customer_email,
      barcode: response.barcode,
      task_status: response.task_status,
      task_datetime: moment(response.task_datetime),
    });
  };
  useEffect(() => {
    if (itemId !== null && visible) getDetailsTask();
  }, [itemId]);

  return (
    <Modal
      title="Details"
      visible={visible}
      onOk={() => {
        formRef.current.validateFields().then((values) => {
          if (values.errorFields === undefined) {
            handleOk(itemId, values);
            formRef.current.resetFields();
          }
        });
      }}
      onCancel={() => {
        handleCancel();
        formRef.current.resetFields();
      }}
    >
      <Form name="basic" ref={formRef}>
        <Form.Item
          label="Customer Name"
          name="customer_name"
          rules={[{ required: true, message: "Please this field required!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Customer phone"
          name="customer_phone"
          rules={[{ required: true, message: "Please this field required!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Customer Email"
          name="customer_email"
          rules={[
            { required: true, message: "Please this field required!" },
            { type: "email", message: "Emailis not a valid email" },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Customer Barcode"
          name="barcode"
          rules={[{ required: true, message: "Please this field required!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Customer status"
          name="task_status"
          rules={[{ required: true, message: "Please this field required!" }]}
        >
          <Select placeholder="Please select" options={STATUS_MAP} />
        </Form.Item>
        <Form.Item
          label="Customer date"
          name="task_datetime"
          rules={[{ required: true, message: "Please this field required!" }]}
        >
          <DatePicker />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default EditModal;
