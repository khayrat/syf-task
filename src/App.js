import React from "react";
import "./App.css";
import { Route } from "react-router-dom";
import Home from "./component/Home/Home";

class BooksApp extends React.Component {
  render() {
    return (
      <div className="app">
        <Route exact path="/" render={() => <Home />} />
      </div>
    );
  }
}

export default BooksApp;
