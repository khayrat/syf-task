export const STATUS_MAP = [
  { value: 0, label: "Assigned" },
  { value: 1, label: "Started" },
  { value: 2, label: "Successful" },
  { value: 3, label: "Failed" },
  { value: 4, label: "InProgress" },
  { value: 6, label: "Unassigned" },
  { value: 7, label: "Accepted" },
  { value: 8, label: "Decline" },
  { value: 9, label: "Cancel" },
  { value: 10, label: "Deleted" },
];
