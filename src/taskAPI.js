import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const api = "http://localhost:3200/tasks";

const toastConfigObject = {
  position: "top-center",
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
};

toast.configure(toastConfigObject);

export const getAllTask = async () => {
  const response = await fetch(api);
  if (response.status === 200) return response.json();
  else {
    toast.error("Error on load all data");
  }
};

export const getSingleTask = async (id) => {
  const response = await fetch(`${api}/${id}`);
  if (response.status === 200) return response.json();
  else {
    toast.error("Error on load data");
  }
};

export const deleteSingleTask = async (id) => {
  const response = await fetch(`${api}/${id}`, {
    method: "DELETE",
  });
  if (response.status === 200) {
    toast.success("Your Item deleted success");

    return true;
  } else {
    toast.error("Error on load data");
    return false;
  }
};

export const searchTask = async (values) => {
  const response = await fetch(
    `${api}?${values
      .map((item) => {
        return `task_status=${item}`;
      })
      .join("&")}`,
    {
      method: "GET",
    }
  );
  if (response.status === 200) return response.json();
  else {
    toast.error("Error on load data");
  }
};

export const updateTask = async (id, values) => {
  const response = await fetch(`${api}/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(values),
  });
  if (response.status === 200) {
    toast.success("Your Item update success");
    return true;
  } else {
    toast.error("Error on load data");
    return false;
  }
};
