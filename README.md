# YFS Front-end Exercise Project :

The app should do the following:

Show an index of tasks. This will display a list of tasks in a table view displaying the following columns

- Task ID
- Customer Name
- Customer Phone
- Email.
- Task Status => The Status name of the task NOT the Status ID
- Task Date => In DD-MM-YYYY hh:mm tt Format
- View Button => Will display a popup view containing the rest of the task details in a readonly view, not editable
- Edit Button => Display an editable screen that contains the details of the selected task.
- Delete Button => Delete the selected task from the list

Use appropriate icons for the action columns, no text to be displayed.

Display some kind of 'Loading' indicator while the app is waiting for data to be fetched.

Add filters to the list screen to filter tasks by dates and/or status.

Use the attached files for a list of sample tasks and the status names.

## How To Run:

first run server with json server using this command `json-server --watch db.json --port=3200`

To get started developing right away:
- install all project dependencies with `npm install`
- start the development server with `npm start`

## Technologies Used:

- ReactJS
- HTML5
- CSS3
- JavaScript
